from pyspark import SparkConf, SparkContext

import sys, json
## Constant
APP_NAME = "Listing Counter"
S3BUCKET = "airepaisa"

def toN(line):
    return json.loads(line)["n"]

def main(sc, s3Prefix):
   textRDD = sc.textFile("s3n://" + S3BUCKET + "/" + s3Prefix)
   print textRDD.map(toN).reduce(lambda a, b: a+b)

if __name__ == "__main__":
   conf = SparkConf().setAppName(APP_NAME)
   sc   = SparkContext(conf=conf)
   filename = sys.argv[1]
   main(sc, filename)
