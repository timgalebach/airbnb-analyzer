import sys, json, boto3, time
from pyspark import SparkConf, SparkContext


## Constants
APP_NAME = "Listing Counter"
S3BUCKET = "airepaisa"

def timestamp():
    """returns current time as int, down to millis"""
    return int(time.time()*1000)

if(len(sys.argv) != 1):
    print("python booking.py")
    sys.exit(0)

def writeBooking(b):
    dynamodb = boto3.resource('dynamodb', region_name="us-east-1")
    table = dynamodb.Table('bookingInfo')
    if timestamp() % 1000 == 0:
        print "heartbeat"

    try:
        table.put_item(Item=b)
    except ResourceNotFoundException as e:
        print "ResourceNotFound"

def dateBreak(dateString):
    return tuple(map(int, dateString.split("-")))

#{"av": 'f', "d": '2016-07-31 '2016-07-31', "p": 500.0}
def getMonths(days):
    months = {}
    for d in days:
        y, m, day = dateBreak(d["d"])
        avPrice = (True if d["av"] == "t" else False, d["p"], d["d"])
        #inserts tuples of (availability, price)
        if (y, m) in months:
            months[(y, m)].append(avPrice)
        else:
            months[(y, m)] = [avPrice]
    newMonths = {}
    for k in months:
        noDups = set(months[k])
        if len(noDups) >= 28:
            newMonths[k] = noDups

    months = newMonths
    return months

def priceStats(months):
    """returns booked price in dollars, occupancy in % for each month"""
    result = {}
    for k in months:
        total = len(months[k])
        newKey = str(k[0]) + ":" + str(k[1])
        booked = filter(lambda x: not x[0], months[k])
        #free = filter(lambda x: x[0], months[k])
        if len(booked) > 0:
            result[newKey] = {"occupancy": int(100*(len(booked)/float(total))),
                         "booked_price": int((reduce(lambda a, b: a+b,
                                                     map(lambda x: x[1], booked)))/float(len(booked)))}
        else:
            result[newKey] = {"occupancy": 0, "booked_price": 0}
    return result

def processBooking(bookingString):
    b = json.loads(bookingString)
    id = b["id"]
    ts = b["ts"]
    days = b["days"]
    months = priceStats(getMonths(days))
    return {"id": id, "ts": ts, "months": months}

prefixes = ["BOOKING_INFO/all/2016-9-26", "BOOKING_INFO/all/2016-9-27", "BOOKING_INFO/all/2016-9-28", "BOOKING_INFO/all/2016-9-29", "BOOKING_INFO/all/2016-9-30"]

def main(sc, bucket, prefixes):
    textRDDs = []
    for prefix in prefixes:
        textRDDs.append(sc.textFile("s3n://" + bucket + "/" + prefix + "*"))
    sc.union(textRDDs).map(processBooking).foreach(writeBooking)
#   print textRDD.take(1)

if __name__ == "__main__":
   # Configure Spark
   conf = SparkConf().setAppName(APP_NAME)
   sc   = SparkContext(conf=conf)
   main(sc, "airepaisa", prefixes)
