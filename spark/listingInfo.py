import sys, json, boto3, time
from pyspark import SparkConf, SparkContext


## Constants
APP_NAME = "Listing Info Rewrite"
S3BUCKET = "airepaisa"

def timestamp():
    """returns current time as int, down to millis"""
    return int(time.time()*1000)

def writeToDynamo(listing):
    listing = json.loads(listing)
    dynamodb = boto3.resource('dynamodb', region_name="us-east-1")
    table = dynamodb.Table('listings2')
    try:
        table.put_item(Item=listing)
    except ResourceNotFoundException as e:
        print "ResourceNotFound"

if(len(sys.argv) != 2):
    print("python listingInfo.py S3_PREFIX")
    sys.exit(0)

def flipBedsBaths(line):
    o = json.loads(line)
    beds = o["baths"]
    baths = o["beds"]
    o["beds"] = beds
    o["baths"] = baths
    return json.dumps(o)

prefixes = ["LISTING_INFO/all/2016-9-18", "LISTING_INFO/all/2016-9-19", "LISTING_INFO/all/2016-9-20", "LISTING_INFO/all/2016-9-21"]

def main2(sc, s3Prefix):
    outfile = "s3n://" + S3BUCKET + "/LISTING_INFO/all/2016-9-24"
    textRDDs = []
    for prefix in prefixes:
        textRDDs.append(sc.textFile("s3n://" + S3BUCKET + "/" + prefix + "*"))
    print sc.union(textRDDs).map(flipBedsBaths).saveAsTextFile(outfile)

def main(sc, s3Prefix):
    textRDD = sc.textFile("s3n://" + S3BUCKET + "/" + s3Prefix)
    textRDD.foreach(writeToDynamo)

if __name__ == "__main__":
   # Configure Spark
   conf = SparkConf().setAppName(APP_NAME)
   sc   = SparkContext(conf=conf)
   filename = sys.argv[1]

   main(sc, filename)
