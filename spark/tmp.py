#from pyspark import SparkConf, SparkContext

import sys, json, boto3, decimal, sys
## Constants
APP_NAME = "Listing Counter"
S3BUCKET = "airepaisa"

def printflush(s):
    print s
    sys.stdout.flush()

def scanAll(table, lastKey):
    if lastKey:
        return table.scan(Select='ALL_ATTRIBUTES', Limit=50000,
              #            ProjectionExpression='id, lat, lng',
                          ExclusiveStartKey=lastKey)
    else:
        return table.scan(Select='ALL_ATTRIBUTES', Limit=50000,
                          #ProjectionExpression='id, lat, lng'
                          )

def idKeyInt(key):
    """takes a key  {u'id': Decimal('10654763')}
    returns {"id": 10654763}"""
    return {"id": int(key['id'])}

def setTableItem(table, id, item):
    """updates all fields in table at id to be item"""
    ue = "SET "
    ean = {}
    eav = {}
    for key in item:
        eanIndex = "#" + key
        valIndex = ":" + key
        ean[eanIndex] = key
        eav[valIndex] = item[key]
        ue += eanIndex + " = " + valIndex + ", "
    ue = ue[:-2]
    table.update_item(Key={'id': decimal.Decimal(id)},
                      UpdateExpression=ue, ExpressionAttributeValues=eav,
                      ExpressionAttributeNames=ean)


"""

def dateBreak(dateString):
    return tuple(map(int, dateString.split("-")))

def getMonths(days):
    months = {}
    for d in days:
        y, m, day = dateBreak(d["d"])
        avPrice = (True if d["av"] == "t" else False, d["p"], d["d"])
        #inserts tuples of (availability, price)
        if (y, m) in months:
            months[(y, m)].append(avPrice)
        else:
            months[(y, m)] = [avPrice]
    newMonths = {}
    for k in months:
        noDups = set(months[k])
        if len(noDups) >= 28:
            newMonths[k] = noDups

    months = newMonths
    return months

def priceStats(months):
    result = {}
    for k in months:
        total = len(months[k])
        booked = filter(lambda x: not x[0], months[k])
        free = filter(lambda x: x[0], months[k])
        result[k] = {"occupancy": int(100*(len(booked)/float(total))),
                     "booked_price": int((reduce(lambda a, b: a+b,
                                             map(lambda x: x[1], booked)))/float(len(booked)))}
    return result

def processBooking(bookingString):
    b = json.loads(bookingString)
    id = b["id"]
    ts = b["ts"]
    days = b["days"]
    months = priceStats(getMonths(days))
    return {"id": id, "ts": ts, "months": months}

"""
