import sys, json, boto3, time, pika, re
from pyspark import SparkConf, SparkContext

APP_NAME = "Generate Listing Pages"
S3BUCKET = "airepaisa"

if(len(sys.argv) != 3):
    print("python listingInfo.py RABBIT_IP S3_PREFIX")
    sys.exit(0)

rabbitIp = sys.argv[1]
s3Prefix = sys.argv[2]
listingPageQ = "LISTING_PAGE"
listingQ = "LISTING"

def sendToRabbit(urlList):
    while True:
        try:
            connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitIp))
            channel = connection.channel()
            channel.queue_declare(queue=listingPageQ)
            urlList = json.loads(urlList)
            for u in urlList:
                msg = json.dumps({"dataType": "LISTINGS_URL", "data": u})
                channel.basic_publish(exchange='', routing_key=listingPageQ, body=msg)
            connection.close()
            break
        except pika.exceptions.ConnectionClosed as e:
            pass #try again

def idToRabbitBookingInfo(id):
    while True:
        try:
            connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitIp))
            channel = connection.channel()
            channel.queue_declare(queue=listingQ)
            msg = json.dumps({"dataType": "BOOKING_INFO", "data": {"id": str(id)}})
            channel.basic_publish(exchange='', routing_key=listingQ, body=msg)
            connection.close()
            break
        except pika.exceptions.ConnectionClosed as e:
            pass #try again



def safeFloat(x):
    return '%.014f' % x

def urlFromCoords(coordsTuple):
    c = tuple([safeFloat(x) for x in coordsTuple])
    return "https://www.airbnb.com/s/US?guests=1&search_by_map=true&sw_lat=%s&sw_lng=%s&ne_lat=%s&ne_lng=%s&ss_id=1ffzj80d&page=1&source=map&airbnb_plus_only=false&s_tag=YEbEYgcp" % c

def generateListingPageUrls(line):
    o = json.loads(line)
    n = o["n"]
    coords = o["coords"]
    url = urlFromCoords(coords)
    numPages = n/18 if n % 18 == 0 else n/18 + 1
    pageUrlList = []
    for i in range(numPages):
        pageUrl = re.sub(r"&page.[0-9]*", "", url)
        pageUrl += "&page=" + str(i+1)
        pageUrlList.append(pageUrl)
    return json.dumps(pageUrlList)

def getListingIdsFromRegions(sc, s3Prefix):
    textRDD = sc.textFile("s3n://" + S3BUCKET + "/" + s3Prefix)
    textRDD.map(generateListingPageUrls).foreach(sendToRabbit)

def flatten(list):
    return [item for sublist in list for item in sublist]

def uniqueIdsFromListingIds(sc, s3Prefix):
    """s3Prefix should be to LISTING_ID"""
    textRDD = sc.textFile("s3n://" + S3BUCKET + "/" + s3Prefix)
    print len(set(flatten(textRDD.map(lambda line: json.loads(line)["ids"]).collect())))

def sendIdsToBookingInfo(sc, s3Prefix):
    textRDD = sc.textFile("s3n://" + S3BUCKET + "/" + s3Prefix)
    ids = set(flatten(textRDD.map(lambda line: json.loads(line)["ids"]).collect()))
    sc.parallelize(ids).foreach(idToRabbitBookingInfo)

if __name__ == "__main__":
   # Configure Spark
   conf = SparkConf().setAppName(APP_NAME)
   sc   = SparkContext(conf=conf)
   sendIdsToBookingInfo(sc, s3Prefix)
