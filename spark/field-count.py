from pyspark import SparkConf, SparkContext

import sys, json
## Constants
APP_NAME = "Listing Counter"
S3BUCKET = "airepaisa"

if(len(sys.argv) != 2):
    printflush("python tmp.py S3_PREFIX")
    sys.exit(0)

def second(tup):
    return tup[1]

def latLngDict(line):
    o = json.loads(line)
    try:
        lat = int(float(o["lat"]))
        lng = int(float(o["lng"]))
        return ((lat, lng), 1)
    except:
        return ("none", 0)

def fieldTupleFn(key):
    """returns a fn that puts the val as key, 1 as val, in a tuple"""
    def g(line):
        val = json.loads(line)[key]
        return (val, 1)
    return g

def keyValTupleFn(key):
    """returns a fn that puts the val as key, 1 as val, in a tuple"""
    def g(line):
        val = json.loads(line)[key]
        return (key, int(val))
    return g

def latLngGrouping(sc, s3Prefix):
   textRDD = sc.textFile("s3n://" + S3BUCKET + "/" + s3Prefix + "/*")
   ll = textRDD.map(latLngDict)
   red = ll.reduceByKey(lambda a, b: a+b).map(lambda a: (a[1], a[0])).sortByKey(False)
   print red.take(20)

def main(sc, s3Prefix):
   textRDD = sc.textFile("s3n://" + S3BUCKET + "/" + s3Prefix + "/*")
   with_keys = textRDD.map(fieldTupleFn("numReviews"))
# print textRDD.take(1)
   red = with_keys.reduceByKey(lambda a, b: a + b)
   print sorted(red.collect(), key=second, reverse=True)

if __name__ == "__main__":

   # Configure Spark
   conf = SparkConf().setAppName(APP_NAME)
#   conf = conf.setMaster("local[*]")
   sc   = SparkContext(conf=conf)
#   sc._jsc.hadoopConfiguration().set("mapreduce.input.fileinputformat.input.dir.recursive", "true")
   filename = sys.argv[1]
   latLngGrouping(sc, filename)
#   main(sc, filename)
