#!/usr/bin/env python
###TAKES "DONE" body OR->
###TAKES json messages of the form:
###{"dataType": TYPE, "data"=JSON_STRING}
###dataTypeS: "COORDS", "LISTING_ID", "LISTING_INFO", "BOOKING_INFO"
#######
###json dumps results to OUTFILE when finished

###TYPE SPECS:
###COORDS: {'coords': coordinate_array, 'n': NUM_LISTINGS}
###LISTING_ID: {'ids': ID_LIST AS INTS, 'coords': coordinate_array}

import pika, sys, json, boto3, time, uuid

def printflush(s):
    print s
    sys.stdout.flush()

if(len(sys.argv) != 5):
    print "python data-warehouse.py HOST_IP MAX_ELEMENTS DATA_QUEUE S3_BUCKET"
    sys.exit(0)
hostIp  = sys.argv[1]
MAX_ELEMENTS  = int(sys.argv[2])
qu = sys.argv[3]
bucket = sys.argv[4]

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=hostIp))
channel = connection.channel()
channel.queue_declare(queue=qu)

def lowerLeft(coords):
    """returns sw_lat and sw_lng as string, rounded down to int: 'lat-lng' """
    return str(int(coords[0])) + "&" + str(int(coords[1]))

def coordsName(data):
    return "all"

def listingIdName(data):
    return "all"

def listingInfoName(data):
    return "all"

def bookingInfoName(data):
    return "all"

###global vars
groups = {"COORDS": {}, "LISTING_ID": {}, "LISTING_INFO": {}, "BOOKING_INFO": {}}
s3NameFns = {"COORDS": coordsName, "LISTING_ID": listingIdName,
             "LISTING_INFO": listingInfoName, "BOOKING_INFO": bookingInfoName}

def listToS3(prefix, lst):
    """sends a list of objects to s3 as lines of json"""
    t = time.gmtime()
    #uses timestamp as the final filename
    d = prefix + "/" + str(t.tm_year) + "-" + str(t.tm_mon) + "-" + str(t.tm_mday) + "/" + str(int(time.time()*1000))
    s3 = boto3.resource("s3")
    outStr = ""
    for e in lst:
        outStr += json.dumps(e) + "\n"
    s3.Bucket(bucket).put_object(Key=d, Body=outStr)

def flushAllToS3(groups):
    for dataType in groups:
        for key in groups[dataType]:
            prefix = dataType + "/" + key
            listToS3(prefix, groups[dataType][key])

def totalAndMaxElements(groups):
    """returns {'total': TOTAL_LIST_ELEMENTS, 'maxGroup': ["COORDS", "20&-87" 30]"""
    total = 0
    maxGroup = ["", "", -1]
    for dataType in groups:
        for key in groups[dataType]:
            amount = len(groups[dataType][key])
            total += amount
            if(amount > maxGroup[2]):
                maxGroup = [dataType, key, amount]
    return {'total': total, 'maxGroup': maxGroup}

def insert(groups, dataType, key, val):
    """appends val to the list located at dict[key]"""
    if(key not in groups[dataType]):
        groups[dataType][key] = []
    groups[dataType][key].append(val)

def handleMsg(msg):
    global groups

    data = json.loads(msg["data"])
    dataType = msg["dataType"]

    s3Name = s3NameFns[dataType](data)
    insert(groups, dataType, s3Name, data)

    ###if too many elements in groups, flush biggest group
    tam = totalAndMaxElements(groups)
    if(tam['total'] > MAX_ELEMENTS):
        dataType = tam['maxGroup'][0]
        key = tam['maxGroup'][1]
        prefix = dataType + "/" + key
        printflush("writing " + dataType + " " + key + " to s3")
        listToS3(prefix, groups[dataType][key])
        del(groups[dataType][key])

def callback(ch, method, properties, body):
    if(body == "DONE"):
        printflush("received DONE message, flushing data to s3")
        flushAllToS3(groups)
        sys.exit(0)

    printflush("rec msg")
#    printflush(" [x] Received %r" % body)
    handleMsg(json.loads(body))

channel.basic_consume(callback, queue=qu, no_ack=True)

printflush(' [*] Data Warehouse waiting for messages.')
channel.start_consuming()
