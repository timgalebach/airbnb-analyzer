#!/usr/bin/env python
###CURRENTLY TURNED OFF
####notifies the data warehouse when a job is done
###takes json message of the form:
### {'group': UUID, 'item': UUID, 'op': ADD|RM, 'workQu': QUEUE_ID, 'destQu: QUEUE_ID}

import pika, sys, json, uuid

def printflush(s):
    print s
    sys.stdout.flush()

if(len(sys.argv) != 3):
    print "python rabbit-receive.py HOST_IP CONTROL_QUEUE"
    sys.exit(0)

hostIp  = sys.argv[1]
qu = sys.argv[2]

remainingGroups = {}
connection = pika.BlockingConnection(pika.ConnectionParameters(host=hostIp))
channel = connection.channel()

channel.queue_declare(queue=qu)

def runOp(msg, groupDict):
    """returns the new groupDict after running ADD or RM on it"""
    opCode = msg['op']
    itemKey = msg['item']
    if(opCode == "ADD"):
        groupDict[itemKey] = 1;
        return groupDict
    elif(opCode == "RM"):
        if(itemKey in groupDict):
            groupDict.pop(itemKey)
        return groupDict
    else:
        return "invalid opCode"

###takes json message of the form:
### {'group': UUID, 'item': UUID, 'op': ADD|RM, 'workQu': QUEUE_ID, 'destQu: QUEUE_ID}
### if ADD, it pushes the item to the group, if RM, pops it
### if op was RM, checks if list is empty. If empty, tells DATA_W_QUEUE to stop processing it
### TODO: send kill signal to originating queue
def callback(ch, method, properties, body):
    global remainingGroups, channel
    msg = json.loads(body)
    currGroup = msg['group']
    workQu = msg['workQu']
    destQu = msg['destQu']

    if (currGroup in remainingGroups):
        remainingGroups[currGroup] = runOp(msg, remainingGroups[currGroup])
    else:
        remainingGroups[currGroup] = runOp(msg, {})

    #if the operation emptied out the group, send a message
    if(not any(remainingGroups[currGroup])):
        printflush("GROUP %s is DONE PROCESSING, NOT SENDING MSG" % currGroup)
        #destMsg = json.dumps({"groupId": currGroup, "opCode": "DONE",
                              "data": json.dumps("done with job: "+workQu)})
        #channel.basic_publish(exchange='', routing_key=destQu, body=destMsg)

    ###acknowledge processing of message
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_consume(callback, queue=qu)

printflush(' [*] Waiting for messages to Controller. To exit press CTRL+C')
channel.start_consuming()
