import urllib2, sys, re, time, json, pika, utils, boto3, botocore
###takes message of the form:
# {"dataType": TYPE, "data": DATA}
# TYPE: LISTING_ID | DB_LISTING | BOOKING_INFO | HTTP_RESULT
# LISTING_ID data is a list of ids
# DB_LISTING is {"id": LISTING_ID, ...other fields}
# BOOKING_INFO: {"ids": [list of ids]}
##
# should use LISTING as the queue

#####################
#  Constants
####################
LISTING_URL_FRONT = "https://www.airbnb.com/rooms/"
LISTING_URL_BACK = "?checkin=&checkout=&guests=1&s=TYF5h8W9&sug=50"

###calendar url
cal = "https://www.airbnb.com/api/v2/calendar_months?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=USD&locale=en&listing_id=4136875&month=6&year=2016&count=3&_format=with_conditions"

##################################
# REGEXS to get listing info
##################################
regexs = [
    {"resultPrefix": "accom", "method": "MATCH",
     "regex": "Accommodates[^0-9]*([0-9]*).*labe"},
    {"resultPrefix": "baths", "method": "MATCH",
     "regex": "label...Bathrooms[^0-9]*([0-9]*).*lab"},
    {"resultPrefix": "beds", "method": "MATCH",
     "regex": "label...Bedrooms[^0-9]*([0-9]*).*labe"},
    {"resultPrefix": "numBeds", "method": "MATCH",
     "regex": "Beds:.,.label_url.:null,.value.:.([0-9]*)"},
    {"resultPrefix": "propertyType", "method": "MATCH",
     "regex": ".Property.type.*?value...([^,]*).,.*?labe"},
    {"resultPrefix": "roomType", "method": "MATCH",
     "regex": "..Room.type.*?value...([^,]*).,"},

    {"resultPrefix": "starRating", "method": "MATCH",
     "regex": ".star_rating.:([^,]*),"},
    {"resultPrefix": "numReviews", "method": "MATCH",
     "regex": "localized_visible_review_count.:.([0-9]*).reviews"},
    {"resultPrefix": "minNights", "method": "MATCH",
     "regex": ".min_nights.:([0-9]*),"},
    {"resultPrefix": "lat", "method": "MATCH",
     "regex": "offset_lat[^:]*:([^,]*),"},
    {"resultPrefix": "lng", "method": "MATCH",
     "regex": "offset_lng[^:]*:([^,]*).,"},

    {"resultPrefix": "hostName", "method": "MATCH",
     "regex": "host_name...([^,]*).,"},
    {"resultPrefix": "hostId", "method": "MATCH",
     "regex": "host_name...[^,]+.,.id..([0-9]*)"},
    {"resultPrefix": "listingName", "method": "MATCH",
     "regex": "summaryTitle...(.*?)...summaryAddress"},
    {"resultPrefix": "cleaningFee", "method": "MATCH",
     "regex": ":.Cleaning Fee:.,.label_url.:null,.value.:..([0-9]*)"},
    {"resultPrefix": "cancel", "method": "MATCH",
     "regex": "cancellation_policy_label...([^,]*).,.cancellation_polic"},
    {"resultPrefix": "lastReview", "method": "MATCH",
     "regex": "sorted_review..*?localized_date.:.([^,]*).,"},
]

def printflush(s):
    print s
    sys.stdout.flush()

if(len(sys.argv) != 7):
    printflush("python listing-page.py HOST_IP LISTING_QUEUE DATAW_QUEUE HTTP_QUEUE MONTHS_BACK TOTAL_MONTHS")
    sys.exit(0)

###START RABBITmq DECLARATIONS#####
hostIp  = sys.argv[1]
qu = sys.argv[2]
dataWQu = sys.argv[3]
httpQueue = sys.argv[4]

MONTHS_BACK = int(sys.argv[5])
TOTAL_MONTHS = int(sys.argv[6])

dd = boto3.resource('dynamodb')
table = dd.Table('listings2')

connection = pika.BlockingConnection(pika.ConnectionParameters(host=hostIp))
channel = connection.channel()
channel.queue_declare(queue=qu)
channel.queue_declare(queue=dataWQu)
channel.queue_declare(queue=httpQueue)
###END RABBITmq DECLARATIONS########

def handleListingIds(ch, httpQueue, myQueue, idList):
    for id in idList:
        try:
            table.put_item(Item={'id': str(id), 'lastUpdated': 0},
                           ConditionExpression="attribute_not_exists (id)")
            printflush("inserted id %s" % (id))
        except botocore.exceptions.ClientError as e:
            pass

def getDbListing(ch, httpQu, myQu, data, regexs):
    url = LISTING_URL_FRONT + str(data['id']) + LISTING_URL_BACK
    utils.publishMsg(ch, httpQu, utils.airbnbHttpMsg(url, regexs, myQu))

def getBookingInfo(ch, httpQu, myQu, idList, monthsBack, totalMonths):
    for id in idList:
        rs = [{"resultPrefix": "bookingInfo", "method": "FINDALL",
               "regex": "available.:(true|false),.date.:.([^,]+).,.*?local_price.:([^,]+),"}]
        month, year = utils.monthYear(monthsBack)
        url = utils.bookingUrl(id, month, year, totalMonths)
        utils.publishMsg(ch, httpQu, utils.airbnbHttpMsg(url, rs, myQu))

def handleListingInfoResult(results, url, dataQu, myQu, ch, table):
    id = utils.idFromUrl(url)
    item = {}
    for r in results:
        if r['data']:
            item[r['resultPrefix']] = r['data'][0]
        else:
            item[r['resultPrefix']] = None

    #make sure result was actually loaded, no error on page
    if re.search("lease.try.again", item["roomType"]):
        printflush("couldn't load page")
        getDbListing(ch, httpQueue, myQu, {"id": id}, regexs)
        return None

    #write to dynamo
#    utils.setListingInfo(table, id, item)

    #write to data warehouse, add id
    item["ts"] = utils.timestamp()
    item["id"] = id
    utils.publishMsg(ch, dataQu, utils.dataWMsg("LISTING_INFO", item))

def handleBookingInfoResult(ch, url, calendar, dataQu):
    id = utils.idFromBookingUrl(url)
    days = [utils.calendarTupleToDB(d) for d in calendar]
    for d in days:
        if d["p"] is None:
            printflush(id)
    ts = utils.timestamp()
    item = {}
    item["id"] = id
    item["ts"] = ts
    item["days"] = days
    utils.publishMsg(ch, dataQu, utils.dataWMsg("BOOKING_INFO", item))

def handleHttpResult(results, url, dataQu, myQu, ch, table):
    if len(results) == 0:
        return None
    if results[0]["resultPrefix"] == "bookingInfo":
        handleBookingInfoResult(ch, url, results[0]["data"], dataQu)
    else:
        handleListingInfoResult(results, url, dataQu, myQu, ch, table)


def callback(ch, method, properties, body):
    """expects body to be json string of form:
    {"dataType": UUID, "data": json string}"""
    global channel, qu

    msg = json.loads(body)
    dataType = msg["dataType"]

    if dataType == "LISTING_ID":
        handleListingIds(channel, httpQueue, qu, msg['data'])
    elif dataType == "DB_LISTING":
        getDbListing(channel, httpQueue, qu, msg['data'], regexs)
    elif dataType == "BOOKING_INFO":
        getBookingInfo(channel, httpQueue, qu, msg['data'], MONTHS_BACK, TOTAL_MONTHS)
    elif dataType == "HTTP_RESULT":
        handleHttpResult(msg['results'], msg['url'], dataWQu, qu, channel, table)
    else:
        printflush("invalid data")
        return #without acking

    ch.basic_ack(delivery_tag = method.delivery_tag)


###LISTEN FOR INCOMING URLS FROM RABBITMQ###
printflush("Waiting for listings to work with...")
channel.basic_consume(callback, queue=qu)
channel.start_consuming()
