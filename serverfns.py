import boto3, utils, sys, dataviews, redis, json

r = redis.StrictRedis(host='localhost', port=6379, db=0)

def numListings(listingMap):
    total = 0
    for k in listingMap:
        total += len(listingMap[k])
    return total

def loadAllListings(regions, force=False):
    total = 0
    for regionKey in regions:
        getListings(regionKey, force)
        total +=1
        if total % 100 == 0:
            utils.printflush(total)

def getRegions(force=False):
    """put all regions into redis"""
    regions = r.get("REGIONS")
    if regions is None or force:
        print "[*] Fetching regions from dynamo..."
        regions = dataviews.idsByRegion()
        r.set("REGIONS", json.dumps(regions))
        print "[*] All regions loaded."
        return regions
    else:
        return json.loads(regions)

def getListings(regionKey, force=False):
    regions = getRegions()
    if regionKey in regions:
        res = r.get("LISTINGS:" + regionKey)
        if res is not None and not force:
            return json.loads(res)
        else:
            regionIds = regions[regionKey]
            utils.printflush("[*] Fetching listings from dynamo for %s ..." % (regionKey))
            listings = dataviews.fullListings(regionIds)
            r.set("LISTINGS:" + regionKey, json.dumps(listings))
            return listings
    else:
        return []

def regionStrToTuple(regionStr):
    r = regionStr.split(":")
    return (int(r[0]), int(r[1]))

def getRegionListings(lat, lng):
    """gets all listings in the given region lat and lng
    gets from dynamo if not cached"""
    key = str(lat) + ":" + str(lng)
    return getListings(key)

def getRegionCounts():
    """returns map {"region coords": numListings}"""
    regions = getRegions()
    ret = {}
    for k in regions:
        ret[k] = len(regions[k])
    return ret
