#!/usr/bin/env python
import pika, sys

if(len(sys.argv) != 3):
    print "python rabbit-receive.py HOST_IP QUEUE_NAME"
    sys.exit(0)

hostIp  = sys.argv[1]
qu = sys.argv[2]

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=hostIp))
channel = connection.channel()

channel.queue_declare(queue=qu)


#NOTE: any sleep in here DOES block the function
def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)

channel.basic_consume(callback, queue=qu, no_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
