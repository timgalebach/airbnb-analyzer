import urllib2, sys, re, time, random, json, pika, uuid, copy, sets, utils
###takes message of the form:
##  {"dataType": type, "data": python object}
# dataType is  LISTINGS_URL
#data for LISTINGS_URL: a url


def printflush(s):
    print s
    sys.stdout.flush()

if(len(sys.argv) != 5):
    printflush("python listing-page.py HOST_IP LISTING_PAGE_QUEUE DATA_W_QUEUE HTTP_QUEUE")
    sys.exit(0)

###START RABBITmq DECLARATIONS#####
hostIp  = sys.argv[1]
qu = sys.argv[2]
dataWQu = sys.argv[3]
httpQueue = sys.argv[4]

connection = pika.BlockingConnection(pika.ConnectionParameters(host=hostIp))
channel = connection.channel()
channel.queue_declare(queue=qu)
channel.queue_declare(queue=dataWQu)
channel.queue_declare(queue=httpQueue)
###END RABBITmq DECLARATIONS########

def coordsFromUrl(url):
    m = re.search(r"sw_lat=(.*[^&]).*sw_lng=(.*[^&]).*ne_lat=(.*[^&]).*ne_lng=(\-?[0-9]*\.[0-9]*[^&])", url)
    return tuple([float(s) for s in m.groups()])

def handleListingUrl(ch, httpQueue, myQueue, url):
    nextPageRe = "<li.class..next.next_pag.[^<]+<a.href=.*?page=([0-9]*)"
    listingIdsRe = "a.href=..rooms.([0-9]+)[^0-9]"

    rs = [{"regex": listingIdsRe, "resultPrefix": "LISTING_IDS",
           "method": "FINDALL"}]

    utils.publishMsg(ch, httpQueue, utils.airbnbHttpMsg(url, rs, myQueue))

def handleHttpResult(results, url, dataWQu, myQu, ch):
    for res in results:
        if res["resultPrefix"] == "LISTING_IDS":
            listingIds = map(int, list(sets.Set(res["data"])))
            coords = coordsFromUrl(url)
            #write the ids to data warehouse
            utils.publishMsg(ch, dataWQu,
                             utils.dataListingIdMsg(coords, listingIds))

def callback(ch, method, properties, body):
    """expects body to be json string of form:
    {"dataType": UUID, "data": json string}"""
    global channel, qu

    msg = json.loads(body)
    dataType = msg["dataType"]

    lastCall = time.time()
    if dataType == "LISTINGS_URL":
        handleListingUrl(channel, httpQueue, qu, msg['data'])
    elif dataType == "HTTP_RESULT":
        handleHttpResult(msg['results'], msg['url'], dataWQu, qu, channel)
    else:
        printflush("invalid data")
        return #without acking

    ch.basic_ack(delivery_tag = method.delivery_tag)

###LISTEN FOR INCOMING URLS FROM RABBITMQ###
printflush("Listing Page waiting for coords to work with...")
channel.basic_consume(callback, queue=qu)
channel.start_consuming()
