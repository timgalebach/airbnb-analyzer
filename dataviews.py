import boto3, utils, sys

client = boto3.client("dynamodb")
dynamodb = boto3.resource('dynamodb', region_name="us-east-1")

def url(id):
    LISTING_URL_FRONT = "https://www.airbnb.com/rooms/"
    LISTING_URL_BACK = "?checkin=&checkout=&guests=1&s=TYF5h8W9&sug=50"
    return LISTING_URL_FRONT + str(id) + LISTING_URL_BACK

def idsByRegion(swlat, swlng, nelat, nelng):
    """lat and lng should be ints"""
    ids = []
    for lat in range(swlat, nelat+1):
        for lng in range(swlng, nelng+1):
            latlng = str(lat) + ":" + str(lng)
            res = client.get_item(TableName="regions", Key={"latlng": {"S": latlng}})
            if "Item" in res and "ids" in res["Item"] and "L" in res["Item"]["ids"]:
                ids += map(lambda e: e["S"], res["Item"]["ids"]["L"])
    return ids

def listingById(id):
    table = dynamodb.Table('listings2')
    pe = "id, starRating, roomType, beds, baths, numReviews, lat, lng"
    i = table.get_item(Key={"id": str(id)},
                       ProjectionExpression=pe)["Item"]
    return i

def bookingInfoById(id):
    table = dynamodb.Table('bookingInfo')
    pe = "months"
    try:
        i = table.get_item(Key={"id": str(id)}, ProjectionExpression=pe)["Item"]
        for k in i["months"]:
            i["months"][k]["occupancy"] = int(i["months"][k]["occupancy"])
            i["months"][k]["booked_price"] = int(i["months"][k]["booked_price"])
        return i
    except KeyError as e:
        return {"months": None}

def allInfo(id):
    return dict(listingById(id).items() + bookingInfoById(id).items())

def fullListings(idList):
    return map(allInfo, idList)

def idsByRegion():
    """returns map of form {"40:-73": [id1, id2]} """
    table = boto3.resource('dynamodb').Table("regions")
    lastKey = False
    regions = {}
    response = {}
    while True:
        if lastKey:
            response = table.scan(Select='ALL_ATTRIBUTES', ExclusiveStartKey=lastKey)
        else:
            response = table.scan(Select='ALL_ATTRIBUTES')

        lastKey = response['LastEvaluatedKey'] if 'LastEvaluatedKey' in response else None
        utils.printflush(len(response["Items"]))

        for i in response["Items"]:
            regions[i["latlng"]] = i["ids"]

        if lastKey == None:
            break

    return regions
