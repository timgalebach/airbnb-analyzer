########################################
##group-listings.py

import sys, boto3, pika, json, utils


def printflush(s):
    print s
    sys.stdout.flush()

if(len(sys.argv) != 5):
    printflush("python group-listings.py")
    sys.exit(0)

###START RABBITmq DECLARATIONS#####
client = boto3.client('s3')

connection = pika.BlockingConnection(pika.ConnectionParameters(host=hostIp))
channel = connection.channel()
channel.queue_declare(queue=processQu)
channel.queue_declare(queue=listingIdQu)
channel.queue_declare(queue=listingInfoQu)
###END RABBITmq DECLARATIONS########

def coordsToMsg(url):
    """transforms coords to a message for listing-page process"""
    return {'dataType': 'LISTINGS_URL', 'data': url}

def processCoordData(data):
    """expects a file of line-separated coordinate dicts in json string"""
    for s in data.split('\n'):
        if(len(s) > 0 and s[0] == '{'):
            cDict = json.loads(s)
            url = utils.urlFromCoords(cDict['coords'])
            n = cDict["n"]
            if n > 0:
                m = coordsToMsg(url)
                utils.publishMsg(channel, listingIdQu, json.dumps(m))

def processListingIdData(data):
    printflush("starting to process listing ids")
    for s in data.split('\n'):
        if len(s) > 0 and s[0] == '{':
            d = json.loads(s)
            ids = d['ids']
            if len(ids) > 0:
                m = json.dumps({'dataType': 'LISTING_ID', 'data': ids})
                utils.publishMsg(channel, listingInfoQu, m)

def processS3Coords(bucket, prefix):
    objects = utils.getS3BucketObjects(bucket, prefix)
    for o in objects['Contents']:
        data = client.get_object(Bucket=bucket, Key=o['Key'])['Body'].read()
        processCoordData(data)
        printflush("Sent coords in bucket %s and prefix %s to listingId queue." % (bucket, prefix))

def processS3ListingId(bucket, prefix):
    objects = utils.getS3BucketObjects(bucket, prefix)
    printflush("got objects")
    for o in objects['Contents']:
        data = client.get_object(Bucket=bucket, Key=o['Key'])['Body'].read()
        processListingIdData(data)
        printflush("Sent coords in bucket %s and prefix %s to listing info queue." % (bucket, prefix))

def processDynamoListings(listingTable, timeCutoff, listingQu):
    table = boto3.resource('dynamodb').Table(listingTable)
    lastKey = False

    while True:
        response = utils.scanOldListings(table, timeCutoff, lastKey)
        #check whether there's another page to do
        if 'LastEvaluatedKey' in response:
            lastKey = utils.idKeyInt(response['LastEvaluatedKey'])
        else:
            lastKey = None

        items = response['Items']

        printflush("%s items, first id is %s" % (len(items), items[0]['id']))
        for item in items:
            id = utils.idKeyInt(item)['id']
            #process the listing
            msg = json.dumps({"dataType": "DB_LISTING", "data": {"id": id}})
            utils.publishMsg(channel, listingQu, msg)
        if lastKey == None:
            printflush("no lastKey")
            break

def handleMsg(msg):
    dataType = msg["dataType"]
    src = msg['src']

    if dataType == 'COORDS':
        if src['driver'] == 's3':
            processS3Coords(src['bucket'], src['prefix'])
    elif dataType == 'DB_LISTING':
        if src['driver'] == 'dynamo':
            processDynamoListings(src['table'], src['date-to-update-from'], listingInfoQu)
    elif dataType == 'LISTING_ID':
        if src['driver'] == 's3':
            processS3ListingId(src['bucket'], src['prefix'])


def callback(ch, method, properties, body):
    printflush(" [x] Received %r" % body)
    handleMsg(json.loads(body))

channel.basic_consume(callback, queue=processQu, no_ack=True)

printflush(' [*] Processor waiting for messages...')
channel.start_consuming()
