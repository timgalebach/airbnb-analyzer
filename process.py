########################################
##process.py -- take data streams or sources and send them to further scripts
##takes messages of types:
# {'dataType': 'COORDS', 'src': {'driver': 's3', 'bucket': 'airepaisa', 'prefix': 'COORDS/all/}}
# {'dataType': 'LISTING_ID', 'src': {'driver': 's3', 'bucket': 'airepaisa', 'prefix': 'LISTING_ID/all/}}
# {'dataType': 'DB_LISTING', 'src': {'driver': 'dynamo', 'table': 'listings2', 'date-to-update-from': TIMESTAMP_IN_MILLIS, "method": "COUNT"|"PROCESS"}}
# {'dataType': 'GROUP_LISTINGS', 'src': {'driver': 'dynamo', 'table': 'listings2'}}
# {'dataType': 'BOOKING_INFO', 'src': {'driver': 's3', 'bucket': 'airepaisa', "prefix": PREFIX}}
# {'dataType': 'BOOKING_INFO', 'src': {'driver': 'file', 'filename': FILENAME}
# {"dataType": "DB_LISTING", "src": {"driver": "file", "filename": FILENAME}}

##sends coordinate boxes to be turned into listing ids
##sends listing ids to turn into listing_info and listing booking

import sys, boto3, pika, json, utils


def printflush(s):
    print s
    sys.stdout.flush()

if(len(sys.argv) != 5):
    printflush("python process.py RABBIT_IP PROCESS_QUEUE LISTING_PAGE_QUEUE LISTING_QUEUE")
    sys.exit(0)

###START RABBITmq DECLARATIONS#####
hostIp  = sys.argv[1]
processQu = sys.argv[2]
listingIdQu = sys.argv[3]
listingInfoQu = sys.argv[4]

client = boto3.client('s3')

connection = pika.BlockingConnection(pika.ConnectionParameters(host=hostIp))
channel = connection.channel()
channel.queue_declare(queue=processQu)
channel.queue_declare(queue=listingIdQu)
channel.queue_declare(queue=listingInfoQu)
###END RABBITmq DECLARATIONS########

def coordsToMsg(url):
    """transforms coords to a message for listing-page process"""
    return {'dataType': 'LISTINGS_URL', 'data': url}

def processCoordData(data):
    """expects a file of line-separated coordinate dicts in json string"""
    for s in data.split('\n'):
        if(len(s) > 0 and s[0] == '{'):
            cDict = json.loads(s)
            url = utils.urlFromCoords(cDict['coords'])
            n = cDict["n"]
            if n > 0:
                m = coordsToMsg(url)
                utils.publishMsg(channel, listingIdQu, json.dumps(m))

def processIdFile(filename, dest):
    printflush("processing listing file")
    f = open(filename, "r")
    for id in f.read().split("\n"):
        if dest == "BOOKING_INFO":
            m = json.dumps({'dataType': 'BOOKING_INFO', 'data': [id]})
            utils.publishMsg(channel, listingInfoQu, m)
        if dest == "DB_LISTING":
            m = json.dumps({'dataType': 'DB_LISTING', 'data': {"id": id}})
            utils.publishMsg(channel, listingInfoQu, m)

def processListingIdData(data, dest):
    printflush("starting to process listing ids")
    for s in data.split('\n'):
        if len(s) > 0 and s[0] == '{':
            d = json.loads(s)
            ids = d['ids']
            if len(ids) > 0:
                if dest == "DB_LISTING":
                    m = json.dumps({'dataType': 'DB_LISTING', 'data': ids})
                    utils.publishMsg(channel, listingInfoQu, m)
                elif dest == "BOOKING_INFO":
                    m = json.dumps({'dataType': 'BOOKING_INFO', 'data': ids})
                    utils.publishMsg(channel, listingInfoQu, m)

def processS3Coords(bucket, prefix):
    objects = utils.getS3BucketObjects(bucket, prefix)
    for o in objects['Contents']:
        data = client.get_object(Bucket=bucket, Key=o['Key'])['Body'].read()
        processCoordData(data)
        printflush("Sent coords in bucket %s and prefix %s to listingId queue." % (bucket, prefix))

def processS3ListingId(bucket, prefix, dest):
    objects = utils.getS3BucketObjects(bucket, prefix)
    printflush("got objects")
    for o in objects['Contents']:
        data = client.get_object(Bucket=bucket, Key=o['Key'])['Body'].read()
        processListingIdData(data, dest)
        printflush("Sent coords in bucket %s and prefix %s to listing info queue." % (bucket, prefix))

def processDynamoListings(listingTable, timeCutoff, listingQu, action):
    print action
    table = boto3.resource('dynamodb').Table(listingTable)
    lastKey = False

    total = 0
    while True:
        response = utils.scanOldIds(table, timeCutoff, lastKey)
        #check whether there's another page to do
        if 'LastEvaluatedKey' in response:
            lastKey = response['LastEvaluatedKey']
        else:
            lastKey = None

        items = response['Items']
        total += len(items)

        printflush("%s items, first id is %s" % (len(items), items[0]['id']))
        for item in items:
            id = str(id)
            if action is not "COUNT":
                #process the listing
                msg = ""
                if action == "LISTING":
                    msg = json.dumps({"dataType": "DB_LISTING", "data": {"id": id}})
                elif action == "BOOKING":
                    msg = json.dumps({"dataType": "BOOKING_INFO", "data": {"id": id}})

                utils.publishMsg(channel, listingQu, msg)
        if lastKey == None:
            printflush("Processed % items") % (total)
            break

def groupDynamoListings(table):
    table = boto3.resource('dynamodb').Table(listingTable)

def handleMsg(msg):
    dataType = msg["dataType"]
    src = msg['src']

    if dataType == 'COORDS':
        if src['driver'] == 's3':
            processS3Coords(src['bucket'], src['prefix'])
    elif dataType == 'DB_LISTING':
        if src['driver'] == 'dynamo':
            if src['action'] == 'COUNT':
                printflush("counting listings")
                processDynamoListings(src['table'], src['date-to-update-from'], listingInfoQu, "COUNT")
            elif src['action'] == "BOOKING":
                processDynamoListings(src['table'], src['date-to-update-from'], listingInfoQu, "BOOKING")
            elif src['action'] == "LISTING":
                processDynamoListings(src['table'], src['date-to-update-from'], listingInfoQu, "LISTING")
        elif src['driver'] == 'file':
            processIdFile(src['filename'], "DB_LISTING")


    elif dataType == 'LISTING_ID':
        if src['driver'] == 's3':
            processS3ListingId(src['bucket'], src['prefix'], "LISTING_ID")

    elif dataType == 'BOOKING_INFO':
        if src['driver'] == 's3':
            processS3ListingId(src['bucket'], src['prefix'], "BOOKING_INFO")
        elif src['driver'] == 'file':
            processIdFile(src['filename'], "BOOKING_INFO")

    elif dataType == 'GROUP_LISTINGS':
        if src['driver'] == 'dynamo':
            groupDynamoListings(src['table'])


def callback(ch, method, properties, body):
    printflush(" [x] Received %r" % body)
    handleMsg(json.loads(body))

channel.basic_consume(callback, queue=processQu, no_ack=True)

printflush(' [*] Processor waiting for messages...')
channel.start_consuming()
