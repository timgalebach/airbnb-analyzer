import json, pika, urllib2, boto3, time, re, decimal, ua_list, random, datetime, sys

##################################################################
# DYNAMO FNS
def scanOldIds(table, timeCutoff, lastKey):
    """returns all listings in dynamo that have been last updated
    before timeCutoff
    to start at the beginning, set lastKey = False"""
    limit = 30000
    fe = "#L = :zero OR #L < :timeCutoff"
    ean = {"#L": "lastUpdated"}
    eav = {":zero": 0, ":timeCutoff": timeCutoff}

    if lastKey:
        return table.scan(Select='SPECIFIC_ATTRIBUTES', FilterExpression=fe,
                          ProjectionExpression="id",
                          ExpressionAttributeNames=ean,
                          ExpressionAttributeValues=eav,
                          ExclusiveStartKey=lastKey,
                          Limit=limit)
    else:
        return table.scan(Select='SPECIFIC_ATTRIBUTES', FilterExpression=fe, ExpressionAttributeNames=ean, ExpressionAttributeValues=eav, ProjectionExpression="id", Limit=limit)

def setTableItem(table, id, item):
    """updates all fields in table at id to be item"""
    ue = "SET "
    ean = {}
    eav = {}
    for key in item:
        eanIndex = "#" + key
        valIndex = ":" + key
        ean[eanIndex] = key
        eav[valIndex] = item[key]
        ue += eanIndex + " = " + valIndex + ", "
    ue = ue[:-2]
    table.update_item(Key={'id': decimal.Decimal(id)},
                      UpdateExpression=ue, ExpressionAttributeValues=eav,
                      ExpressionAttributeNames=ean)

def setListingInfo(table, id, item):
    ue = "SET "
    ean = {}
    eav = {}
    for key in item:
        eanIndex = "#" + key
        valIndex = ":" + key
        ean[eanIndex] = key
        eav[valIndex] = item[key]
        ue += eanIndex + " = " + valIndex + ", "
    ean["#LU"] = "lastUpdated"
    eav[":LU"] = timestamp()

    ue += "#LU = :LU"
    table.update_item(Key={'id': decimal.Decimal(id)},
                      UpdateExpression=ue, ExpressionAttributeValues=eav,
                      ExpressionAttributeNames=ean)

def idKeyInt(key):
    """takes a key  {u'id': Decimal('10654763')}
    returns {"id": 10654763}"""
    return {"id": int(key['id'])}
#########################################################################

def calendarTupleToDB(ct):
    """converts ['false', '2016-07-31', '500.0'] to
    to {"av": 'f', "d": '2016-07-31', "p": 500.0}
    """
    p = None if ct[2] == "null" else float(ct[2])
    return {"av": ct[0][0], "d": ct[1], "p": p}


###S3 FNS
def prefixDateRange(start, end):
    """start, end tuples of (y, month, day)
    returns a list of strings that are prefixes of those days"""
    start = datetime.date(start[0], start[1], start[2])
    end = datetime.date(end[0], end[1], end[2])
    dates = []
    while not start > end:
        td = datetime.timedelta(days=1)
        dates.append("-".join(map(str, start.timetuple()[0:3])))
        start += td
    return dates


def listToS3(bucket, prefix, lst):
    """sends a list of objects to s3 as lines of json"""
    t = time.gmtime()
    #uses timestamp as the final filename
    d = prefix + "/" + str(t.tm_year) + "-" + str(t.tm_mon) + "-" + str(t.tm_mday) + "/" + str(timestamp())
    s3 = boto3.resource("s3")
    outStr = ""
    for e in lst:
        outStr += json.dumps(e) + "\n"
    s3.Bucket(bucket).put_object(Key=d, Body=outStr)

def getS3BucketObjects(bucket, prefix):
    client = boto3.client('s3')
    return client.list_objects(Bucket=bucket, Prefix=prefix)

###MISC FNS
def getPublicIp():
    return str(urllib2.urlopen('http://myip.dnsomatic.com/').read())

def safeFloat(x):
    return '%.014f' % x

def idFromUrl(url):
    return re.findall("rooms.([0-9]*)", url)[0]

def idFromBookingUrl(url):
    return re.findall("listing.id=([0-9]+)[^0-9]", url)[0]

def bookingUrl(id, startMonth, startYear, numMonths):
    return "https://www.airbnb.com/api/v2/calendar_months?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=USD&locale=en&listing_id=" + str(id) + "&month=" + str(startMonth) + "&year=" + str(startYear) + "&count=" + str(numMonths) + "&_format=with_conditions"

def urlFromCoords(coordsTuple):
    c = tuple([safeFloat(x) for x in coordsTuple])
    return "https://www.airbnb.com/s/US?guests=1&search_by_map=true&sw_lat=%s&sw_lng=%s&ne_lat=%s&ne_lng=%s&ss_id=1ffzj80d&page=1&source=map&airbnb_plus_only=false&s_tag=YEbEYgcp" % c

def timestamp():
    """returns current time as int, down to millis"""
    return int(time.time()*1000)

def monthYear(monthsAgo):
    """returns a tuple of (month, year), where month is 5 is current month is 7 and monthsAgo is 1
    monthsAgo must be 11 or less"""
    m = datetime.datetime.now().month
    y = datetime.datetime.now().year
    delta = m - monthsAgo
    if delta < 1:
        m = 12 + delta
    else:
        m = delta
    return (m, y)

def publishMsg(ch, qu, msg):
    ch.basic_publish(exchange='', routing_key=qu, body=msg)

def regexFailMsg(url, regex):
    return json.dumps({"url": url, "regex": regex})

def airbnbHttpMsg(url, regexs, resultQu):
    """dict: {"dataType": "URL", {"url": URL, "regex": REGEX_STRING, "resultQu": QUEUE_TO_POST_TO}}
    if the dictionary is invalid, returns false"""
    d = {"dataType": "URL", "data": {"url": url, "regexs": regexs, "resultQu": resultQu}}
    return json.dumps(d)

def dataListingIdMsg(coords, listingIds):
    return json.dumps({"dataType": "LISTING_ID",
                       "data": json.dumps({'coords': coords,
                                           'ids': listingIds})})

def dataWMsg(dataType, data):
    return json.dumps({"dataType": dataType, "data": json.dumps(data)})

def itemToDynamo(item):
    for key in item:
        if item[key] == "":
            item[key] = None
    return item

def printflush(s):
    print s
    sys.stdout.flush()

#############################################
####PROXY FNS
#############################################
def getProxyOpener():
    """returns a urllib2 opener with a proxy and random Chrome user-agent"""
    port = str(random.randint(22180, 22199))
    p_url = "62.212.82.84:" + port
    username = 'therandom9'
    password = 'HAXYnFF2TjVAD4qe'
    proxyString = username + ":" + password + "@" + p_url
    proxy  = urllib2.ProxyHandler({'http': proxyString})
    opener = urllib2.build_opener(proxy)
    opener.addheaders = [('User-Agent', ua_list.getUA())]
    return opener

def openPage(url):
    opener = urllib2.build_opener()
    opener.addheaders = [('User-Agent', ua_list.getUA())]
    return opener.open(url).read()
