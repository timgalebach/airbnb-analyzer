#!/usr/bin/env python
import pika, sys

if(len(sys.argv) != 4):
    print "usage: python rabbit-send.py HOST_IP QUEUE_NAME MSG"
    sys.exit(0)

hostIp  = sys.argv[1]
qu = sys.argv[2]
msg=sys.argv[3]

connection = pika.BlockingConnection(pika.ConnectionParameters(host=hostIp))
channel = connection.channel()

channel.queue_declare(queue=qu)

channel.basic_publish(exchange='',
                      routing_key=qu,
                      body=msg)
print(" [x] Sent " + msg)
connection.close()
