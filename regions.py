import urllib2, sys, re, time, random, json, pika, uuid, copy, utils
###takes message of the form:
##  {"dataType": URL, "url": URL}
##also takes HTTP messages
###should only do western or eastern hemisphere at once to avoid weirdness

def printflush(s):
    print s
    sys.stdout.flush()

if(len(sys.argv) != 5):
    print "python rabbit-regions.py RABBIT_IP REGION_QUEUE DATA_W_QUEUE HTTP_QUEUE"
    sys.exit(0)

###START RABBITmq DECLARATIONS#####
hostIp  = sys.argv[1]
qu = sys.argv[2]
dataWQu = sys.argv[3]
httpQueue = sys.argv[4]

connection = pika.BlockingConnection(pika.ConnectionParameters(host=hostIp))
channel = connection.channel()
channel.queue_declare(queue=qu)
channel.queue_declare(queue=dataWQu)
channel.queue_declare(queue=httpQueue)
###END RABBITmq DECLARATIONS########

def coordsFromUrl(url):
    m = re.search(r"sw_lat=(\-?[0-9]*\.[0-9]*).*sw_lng=(\-?[0-9]*\.[0-9]*).*ne_lat=(\-?[0-9]*\.[0-9]*).*ne_lng=(\-?[0-9]*\.[0-9]*)", url)
    if m:
        return tuple([float(s) for s in m.groups()])
    else: #log the error
        printflush("bad url: " + url)
        sys.exit(0)

def splitCoordsHalf(coordsTuple):
    """returns [coordsTuple1, coordsTuple2], each of which has half the latitude
    window of the original."""
    cList = list(coordsTuple)
    latMidpoint = (cList[0]+cList[2])/2
    return [(latMidpoint, cList[1], cList[2], cList[3]),
            (cList[0], cList[1], latMidpoint, cList[3])]

def splitCoordsQuad(coordsTuple):
    """returns [coordsTuple1, coordsTuple2], each of which has half the latitude
    window of the original."""
    cList = list(coordsTuple)
    lngMidpoint = (cList[1]+cList[3])/2
    latMidpoint = (cList[0]+cList[2])/2
    #handle weird python decimal stuff
    if abs(latMidpoint) < 0.0001:
        latMidpoint = 0
    if abs(lngMidpoint) < 0.0001:
        lngMidpoint = 0
    #eliminate duplicates
    return [(latMidpoint, lngMidpoint, cList[2], cList[3]),
            (latMidpoint, cList[1], cList[2], lngMidpoint),
            (cList[0], lngMidpoint, latMidpoint, cList[3]),
            (cList[0], cList[1], latMidpoint, lngMidpoint)]

def handleHttpResult(results, url, dataWQu, myQu, ch):
    for r in results:
        if r["resultPrefix"] == "NUM_LISTINGS":
            n = int(r["data"][0])

            if(n > 299):
                splitUrls = [utils.urlFromCoords(c) for c in splitCoordsQuad(coordsFromUrl(url))]
                splitMsgs = [urlToJsonMsg(u) for u in splitUrls]
                for m in splitMsgs:
                    utils.publishMsg(ch, myQu, m)

            else: #send url to the data warehouse
                dataWMsg = json.dumps({"dataType": "COORDS",
                                       "data": json.dumps({'coords': coordsFromUrl(url), 'n': n})})
                utils.publishMsg(ch, dataWQu, dataWMsg)

def handleUrl(ch, httpQueue, myQueue, url):
    rs = [{"regex": "([0-9]+)[^0-9]?.Rental", "resultPrefix": "NUM_LISTINGS",
           "method": "MATCH"}]
    utils.publishMsg(ch, httpQueue, utils.airbnbHttpMsg(url, rs, myQueue))

def urlToJsonMsg(url):
    return json.dumps({"dataType": "URL", "url": url})

def callback(ch, method, properties, body):
    global channel, qu

    msg = json.loads(body)
    dataType = msg['dataType']
    if dataType == 'URL':
        handleUrl(channel, httpQueue, qu, msg['url'])
    elif dataType == 'HTTP_RESULT':
        handleHttpResult(msg['results'], msg['url'], dataWQu, qu, channel)

    ch.basic_ack(delivery_tag = method.delivery_tag)


###LISTEN FOR INCOMING URLS FROM RABBITMQ###
printflush ("waiting for URLs to process")
channel.basic_consume(callback, queue=qu)
channel.start_consuming()
