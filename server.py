from bottle import Bottle, run, request, static_file
import serverfns as sf, json, sys, utils

if(len(sys.argv) != 2):
    utils.printflush("python server.py ROOT_DIR")
    sys.exit(0)


app = Bottle()
htmlroot = sys.argv[1]

style = "[]"

@app.route("/style")
def mapStyle():
    return style

@app.route("/regions")
def regions():
    """returns all regions with their item counts"""
    return sf.getRegionCounts()

@app.route("/listings", method="POST")
def listingsByRegions():
    listings = []
    if request.json is None:
        return json.dumps(listings)
    else:
        for regionKey in request.json:
            listings += sf.getListings(regionKey)
    return json.dumps(listings)

@app.route('/airbnb/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=htmlroot)

run(app, host='0.0.0.0', port=8080)
