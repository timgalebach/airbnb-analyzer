import boto3, utils, sys, json

dd = boto3.resource('dynamodb')
client = boto3.client('s3')

def scanAll(table, lastKey):
    if lastKey:
        return table.scan(Select='ALL_ATTRIBUTES', Limit=50000,
                          ExclusiveStartKey=lastKey)
    else:
        return table.scan(Select='ALL_ATTRIBUTES', Limit=50000)

def scanIds(table, timeCutoff, lastKey):
    """returns all listings in dynamo that have been last updated
    before timeCutoff
    to start at the beginning, set lastKey = False"""
    limit = 30000
    if lastKey:
        return table.scan(Select='SPECIFIC_ATTRIBUTES',
                          ProjectionExpression="id",
                          ExclusiveStartKey=lastKey,
                          Limit=limit)
    else:
        return table.scan(Select='SPECIFIC_ATTRIBUTES', ProjectionExpression="id", Limit=limit)

def getItem(tableName, id):
    table = boto3.resource('dynamodb').Table(tableName)
    table.get_item(Key={"id": str(id)})

def getBookingIds(bucket, prefix):
    ids = []
    objects = utils.getS3BucketObjects(bucket, prefix)
    for o in objects['Contents']:
        objs =  client.get_object(Bucket=bucket,
                                  Key=o['Key'])['Body'].read()
        bookings = objs.split('\n')
        for x in bookings:
            try:
                x = json.loads(x)
                ids.append(str(x["id"]))
            except:
                pass
    return list(set(ids))

def idSetDifferenceFromFiles(bigFile, smallerFile):
    set1 = []
    set2 = []
    f = open(bigFile, "r")
    for line in f.read().split("\n"):
        set1.append(line)
    f.close()
    f = open(smallerFile, "r")
    for line in f.read().split("\n"):
        set2.append(line)
    f.close()
    set1 = set(set1)
    set2 = set(set2)
    return list(set1.difference(set2))

def idFileSplit(filename, out1, out2):
    f = open(filename, "r")
    ids = []
    for line in f.read().split("\n"):
        ids.append(line)
    f.close()
    mid = len(ids)/2
    f = open(out1, "w")
    for id in ids[0:mid]:
        f.write(id+"\n")
    f.close()
    f = open(out2, "w")
    for id in ids[0:mid]:
        f.write(id+"\n")
    f.close()

def getListingIdIds(bucket, prefix):
    ids = []
    objects = utils.getS3BucketObjects(bucket, prefix)
    for o in objects['Contents']:
        objs =  client.get_object(Bucket=bucket,
                                  Key=o['Key'])['Body'].read()
        lines = objs.split('\n')
        for x in lines:
            try:
                x = json.loads(x)
                ids += x["ids"]
            except:
                pass
    return list(set(ids))

def getListingBookingInfoIds(bucket, prefix):
    ids = []
    objects = utils.getS3BucketObjects(bucket, prefix)
    for o in objects['Contents']:
        objs =  client.get_object(Bucket=bucket,
                                  Key=o['Key'])['Body'].read()
        lines = objs.split('\n')
        for line in lines:
            try:
                ids.append(json.loads(line)["id"])
            except:
                pass
    return list(set(ids))

def idsToFile(filename, ids):
    f = open(filename, "w")
    for id in ids:
        f.write(str(id) + "\n")
    f.close()

def processAllTemplate(dynamoTableName):
    table = boto3.resource('dynamodb').Table(dynamoTableName)
    lastKey = False

    total = 0
    while True:
        response = scanAll(table, lastKey)
        if 'LastEvaluatedKey' in response:
            lastKey = utils.idKeyInt(response['LastEvaluatedKey'])
        else:
            lastKey = None
        items = response['Items']
        total += len(items)
        print("%s items, first id is %s" % (len(items), items[0]['id']))
        for item in items:
            #code to run here
            pass
        if lastKey == None:
            print("Processed %s items") % (total)
            break

def flipBedsBaths(dynamoTableName):
    table = boto3.resource('dynamodb').Table(dynamoTableName)
    lastKey = False

    total = 0
    while True:
        response = scanAll(table, lastKey)
        if 'LastEvaluatedKey' in response:
            lastKey = utils.idKeyInt(response['LastEvaluatedKey'])
        else:
            lastKey = None
        items = response['Items']
        total += len(items)
        print("%s items, first id is %s" % (len(items), items[0]['id']))
        for item in items:
            beds = item["baths"]
            baths = item["beds"]
            utils.setTableItem(table, item["id"], {"beds": beds, "baths": baths})
        if lastKey == None:
            print("Processed %s items") % (total)
            break

def writeDynamoListingsToS3(dynamoTableName, S3Bucket):
    table = boto3.resource('dynamodb').Table(dynamoTableName)
    lastKey = False

    lst = []
    total = 0
    while True:
        response = scanAll(table, lastKey)
        if 'LastEvaluatedKey' in response:
            lastKey = utils.idKeyInt(response['LastEvaluatedKey'])
        else:
            lastKey = None
        items = response['Items']
        total += len(items)

        utils.printflush("%s items, first id is %s" % (len(items), items[0]['id']))
        utils.printflush("Total: %s." % (total))
        for item in items:
            if "lat" in item and item["lat"] is not None:
                item.pop("last-updated", None)
                item.pop("lastUpdated", None)
                item["id"] = str(item["id"])
                lst.append(item)
        #write to s3 if more than 50000 in lst
        if len(lst) > 50000:
            utils.listToS3(S3Bucket, "LISTING_INFO/all", lst)
            utils.printflush("Wrote %s items to S3." % (len(lst)))
            lst = []

        if lastKey == None:
            utils.listToS3(S3Bucket, "LISTING_INFO/all", lst)
            utils.printflush("Wrote %s items to S3." % (len(lst)))
            lst = []
            utils.printflush("Done")
            break

def resetNoneLat(listingTable):
    """sets lastUpdated to 0 when lat is None or not there"""
    table = boto3.resource('dynamodb').Table(listingTable)
    lastKey = False

    total = 0
    while True:
        response = scanAll(table, lastKey)
        #check whether there's another page to do
        if 'LastEvaluatedKey' in response:
            lastKey = utils.idKeyInt(response['LastEvaluatedKey'])
        else:
            lastKey = None

        items = response['Items']
        total += len(items)

        utils.printflush("%s items, first id is %s" % (len(items), items[0]['id']))
        utils.printflush("Total: %s." % (total))
        for item in items:
            if "lat" not in item or item["lat"] is None:
                utils.setTableItem(table, item["id"], {"lastUpdated": 0})

        if lastKey == None:
            utils.printflush("Processed %s items") % (total)
            break

def scanLatLng(table, lastKey):
    if lastKey:
        return table.scan(Select='SPECIFIC_ATTRIBUTES', Limit=50000,
                          ProjectionExpression='id, lat, lng',
                          ExclusiveStartKey=lastKey)
    else:
        return table.scan(Select='SPECIFIC_ATTRIBUTES', Limit=50000,
                          ProjectionExpression='id, lat, lng')

def hashLatLng(latStr, lngStr):
    """truncate to nearest 0.1"""
    lat = latStr.split(".")[0] + "." + latStr.split(".")[1][0]
    lng = lngStr.split(".")[0] + "." + lngStr.split(".")[1][0]
    return lat + ":" + lng

def groupByLatLng(listingTable):
    """returns a map of all regions keyed by lat/lng, with ids in them"""
    table = boto3.resource('dynamodb').Table(listingTable)
    lastKey = False

    r = {}
    total = 0
    while True:
        response = scanLatLng(table, lastKey)
        #check whether there's another page to do
        if 'LastEvaluatedKey' in response:
            lastKey = response['LastEvaluatedKey']
        else:
            lastKey = None

        items = response['Items']
        total += len(items)
#        if total >= 10000:
#            break

        utils.printflush("%s items, first id is %s" % (len(items), items[0]['id']))
        for item in items:
            key = "NONE"
            try:
                if "lat" in item and item["lat"] is not None:
                    key = hashLatLng(item["lat"], item["lng"])
            except:
                print item
                break

            if key == "NONE":
                pass
            elif key not in r:
                r[key] = [str(item["id"])]
            else:
                r[key].append(str(item["id"]))
        if lastKey == None:
            print("Processed %s items") % (total)
            break

    return r

def idToDynamo(id):
    return {'S': id}

def createRegions(regions, tableName):
    """regions is the output from groupByLatLng"""
#    regions = groupByLatLng(listingTable)
    client = boto3.client('dynamodb')
    total = 0
    for key in regions:
        if total % 500 == 0:
            utils.printflush(total)
        client.put_item(TableName=tableName, Item={'latlng': {'S': key},
                                                   'ids': {'L': [idToDynamo(i) for i in regions[key]]}})
        total += 1

def countItems(tableName):
    table = boto3.resource('dynamodb').Table(tableName)
    lastKey = False

    total = 0
    while True:
#        response = utils.scanOldListings(table, 50, lastKey)
        response = scanIds(table, utils.timestamp(), lastKey)
        if 'LastEvaluatedKey' in response:
            lastKey = response['LastEvaluatedKey']
        else:
            lastKey = None
        items = response['Items']
        total += len(items)
        print("%s items, first id is %s" % (len(items), items[0]['id']))
        print total
        for item in items:
            #code to run here
            pass
        if lastKey == None:
            print("Processed %s items") % (total)
            break

def listingS3ToDynamo(bucket, tableName, start, end):
    table = boto3.resource('dynamodb').Table(tableName)
    """start and end are (y, m, d) tuples"""
    prefixList = utils.prefixDateRange(start, end)
    total = 0
    for prefix in prefixList:
        prefix =  "LISTING_INFO/all/" + prefix
        print prefix
        objects = utils.getS3BucketObjects(bucket, prefix)
        for o in objects['Contents']:
            utils.printflush(o["Key"])
            data = client.get_object(Bucket=bucket, Key=o['Key'])['Body'].read()
            if len(data) > 0:
                for line in data.split("\n"):
                    try:
                        item = utils.itemToDynamo(json.loads(line))
                    except ValueError as e:
                        print e
                    total += 1
                    if total % 1000 == 0:
                        utils.printflush(total)
                    table.put_item(Item=item)
